
#if !defined(_SIMPLE_CUBLAS_H_)
#define _SIMPLE_CUBLAS_H_


#include <sstream>
#include <fstream>
#include <stdlib.h>

#include <cuda.h> // need CUDA_VERSION
#include <cudnn.h>

#include "fp16_dev.h"
#include "fp16_emu.h"
#include "gemv.h"
#include "error_util.h"

void copyHostToDeviceMatrices(float*& d_A, float*& d_B,
        float*& d_C, int m, int k, int n, float* h_A, float* h_B, float* h_C);

void callCuBlasSgemm(cublasOperation_t transA, cublasOperation_t transB, int m, int n, int k, float alpha,
        float beta, float* d_A, float* d_B, float* d_C);

void allocateDeviceMatrices(int m, int n, int k, float*& d_A, float*& d_B,
        float*& d_C);

#endif
