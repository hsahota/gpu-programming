/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.  Users and possessors of this source code
 * are hereby granted a nonexclusive, royalty-free license to use this code
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.   This source code is a "commercial item" as
 * that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of
 * "commercial computer  software"  and "commercial computer software
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 *
 * Any use of this source code in individual and commercial software must
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */

/* This example demonstrates how to use the CUBLAS library
 * by scaling an array of floating-point values on the device
 * and comparing the result to the same operation performed
 * on the host.
 */

/* Includes, system */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "simpleCUBLAS.h"


#define M  (1)
#define K  (2400)
#define N  (2560)

/* Host implementation of a simple version of sgemm */
static void simple_sgemm(int n, float alpha, const float *A, const float *B,
        float beta, float *C)
{
    int i;
    int j;
    int k;

    for (i = 0; i < M; ++i) {
        for (j = 0; j < N; ++j) {
            float prod = 0;
            for (k = 0; k < K; ++k) {
                prod += A[k * M + i] * B[j * K + k];
            }
            C[j * M + i] = alpha * prod + beta * C[j * M + i];
        }
    }
}
void callCuBlasSgemm(cublasHandle_t cublasHandle, cublasOperation_t transA, cublasOperation_t transB, int m, int n, int k, float alpha,
        float beta, float* d_A, float* d_B, float* d_C) {
    printf("Multiplying [%d by %d] times [%d by %d].\n", m, k, k, n);

    cublasSgemm_v2(cublasHandle, transA, transB, m, n, k, &alpha, d_A, m, d_B, k, &beta, d_C, m);
}

void allocateDeviceMatrices(int m, int n, int k, float*& d_A, float*& d_B,
        float*& d_C) {
    /* Allocate device memory for the matrices */
    std::cout << "Allocating device matrices" << std::endl;
    printf("Allocating [%d by %d], [%d by %d], and [%d by %d].\n", m, k, k, n,
            m, n);
    checkCudaErrors(cudaMalloc((void ** ) (&d_A), m * k * sizeof(d_A[0])));
    checkCudaErrors(cudaMalloc((void ** ) (&d_B), k * n * sizeof(d_B[0])));
    checkCudaErrors(cudaMalloc((void ** ) (&d_C), m * n * sizeof(d_C[0])));
}

void copyHostToDeviceMatrices(float*& d_A, float*& d_B,
        float*& d_C, int m, int k, int n, float* h_A, float* h_B, float* h_C) {



    checkCudaErrors( cudaMemcpy((void *)d_A, (const void *)h_A,
            m * k * sizeof(h_A[0]),
            cudaMemcpyHostToDevice) );

    checkCudaErrors( cudaMemcpy((void *)d_B, (const void *)h_B,
            k * n * sizeof(h_B[0]),
            cudaMemcpyHostToDevice) );

    checkCudaErrors( cudaMemcpy((void *)d_C, (const void *)h_C,
            m * n * sizeof(h_C[0]),
            cudaMemcpyHostToDevice) );
}

void allocateHostMatrices(float*& h_A, float*& h_B, float*& h_C, int m, int n, int k) {
    /* Allocate host memory for the matrices */
    h_A = (float*) malloc(m * k * sizeof(h_A[0]));
    if (h_A == 0) {
        fprintf(stderr, "!!!! host memory allocation error (A)\n");
        exit(EXIT_FAILURE);
    }
    h_B = (float*) malloc(k * n * sizeof(h_B[0]));
    if (h_B == 0) {
        fprintf(stderr, "!!!! host memory allocation error (B)\n");
        exit(EXIT_FAILURE);
    }
    h_C = (float*) malloc(m * n * sizeof(h_C[0]));
    if (h_C == 0) {
        fprintf(stderr, "!!!! host memory allocation error (C)\n");
        exit(EXIT_FAILURE);
    }
    /* Fill the matrices with test data */
    for (int i = 0; i < m * k; i++) {
        h_A[i] = rand() / (float) RAND_MAX;
        h_A[i] = 0.5 * h_A[i] - 0.25f;
    }
    for (int i = 0; i < k * n; i++) {
        h_B[i] = rand() / (float) RAND_MAX;
        h_B[i] = 0.5 * h_B[i] - 0.25f;
    }
    for (int i = 0; i < m * n; i++) {
        h_C[i] = rand() / (float) RAND_MAX;
    }
}

/* Main */
int main(int argc, char** argv)
{    
    cublasHandle_t cublasHandle;
    float* h_A;
    float* h_B;
    float* h_B_out;
    float* h_B_ref;
    float* h_C;
    float* h_C_ref;
    float* d_A = 0;
    float* d_B = 0;
    float* d_C = 0;
    float alpha = 1.0f;
    float beta = 0.0f;
    int i;
    float error_norm;
    float ref_norm;
    float diff;

    /* Initialize CUBLAS */
    printf("simpleCUBLAS test running..\n");

    checkCublasErrors( cublasCreate(&cublasHandle) );

    allocateHostMatrices(h_A, h_B, h_C, M, N, K);

    /* Allocate device memory for the matrices */
    allocateDeviceMatrices(M, N, K, d_A, d_B, d_C);

    /* Initialize the device matrices with the host matrices */
    copyHostToDeviceMatrices(d_A, d_B, d_C,M, K, N, h_A, h_B, h_C);

    /* Performs operation using plain C code */
    simple_sgemm(N, alpha, h_A, h_B, beta, h_C);
    h_C_ref = h_C;

    /* Performs operation using cublas */
    int m = M, n = N, k = K;
    cublasOperation_t transA = CUBLAS_OP_N, transB = CUBLAS_OP_N;
    callCuBlasSgemm(cublasHandle, transA, transB, m, n, k, alpha, beta, d_A, d_B, d_C);

    /* Allocate host memory for reading back the result from device memory */
    h_C = (float*)malloc(M * N * sizeof(h_C[0]));
    if (h_C == 0) {
        fprintf (stderr, "!!!! host memory allocation error (C)\n");
        return EXIT_FAILURE;
    }

    checkCudaErrors( cudaMemcpy(h_C, d_C,
            M * N * sizeof(h_C[0]),
            cudaMemcpyDeviceToHost) );

    /* Check result against reference */
    error_norm = 0;
    ref_norm = 0;
    for (i = 0; i < M * N; ++i) {
        diff = h_C_ref[i] - h_C[i];
        error_norm += diff * diff;
        ref_norm += h_C_ref[i] * h_C_ref[i];
    }
    error_norm = (float)sqrt((double)error_norm);
    ref_norm = (float)sqrt((double)ref_norm);
    if (fabs(ref_norm) < 1e-7) {
        fprintf (stderr, "!!!! reference norm is 0\n");
        return EXIT_FAILURE;
    }
    printf( "SGEMM Test %s\n", (error_norm / ref_norm < 1e-6f) ? "PASSED" : "FAILED");


    /* Memory clean up */
    free(h_A);
    free(h_B);
    free(h_C);
    free(h_C_ref);

    checkCudaErrors( cudaFree((void*) d_A));
    checkCudaErrors( cudaFree((void*) d_B));
    checkCudaErrors( cudaFree((void*) d_C));

    return EXIT_SUCCESS;
}
