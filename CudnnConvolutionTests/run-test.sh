for inputChannels in 31 32
    do
        for outputChannels in 31 32
            do    
	            echo "input channels = " $inputChannels
                echo "output channels = " $outputChannels
                sed -i -e 's/.*inputChannels =.*/\tinputChannels = '${inputChannels}';/g' ConvolutionTest.cpp
                sed -i -e 's/.*outputChannels =.*/\toutputChannels = '${outputChannels}';/g' ConvolutionTest.cpp
                make
	            nvprof --metrics all -o ./ProfileResults/convolution/ic-${inputChannels}-oc-${outputChannels}.nvprof --print-gpu-trace --profile-api-trace none ./cudnntest
            done
    done
