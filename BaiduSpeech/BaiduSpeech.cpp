/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>

#include <cuda.h> // need CUDA_VERSION
#include <cudnn.h>

#include "ImageIO.h"
#include "fp16_dev.h"
#include "fp16_emu.h"
#include "gemv.h"
#include "error_util.h"

//#include "simpleCUBLAS.h"


// demonstrate different ways of setting tensor descriptor
//#define SIMPLE_TENSOR_DESCRIPTOR
#define ND_TENSOR_DESCRIPTOR
void setTensorDesc(cudnnTensorDescriptor_t& tensorDesc,
        cudnnTensorFormat_t& tensorFormat,
        cudnnDataType_t& dataType,
        int n,
        int c,
        int h,
        int w)
{
#if SIMPLE_TENSOR_DESCRIPTOR
    checkCUDNN( cudnnSetTensor4dDescriptor(tensorDesc,
            tensorFormat,
            dataType,
            n, c,
            h,
            w ) );
#elif defined(ND_TENSOR_DESCRIPTOR)
    const int nDims = 4;
    int dimA[nDims] = {n,c,h,w};
    int strideA[nDims] = {c*h*w, h*w, w, 1};
    checkCUDNN( cudnnSetTensorNdDescriptor(tensorDesc,
            dataType,
            4,
            dimA,
            strideA ) );
#else
    checkCUDNN( cudnnSetTensor4dDescriptorEx(tensorDesc,
            dataType,
            n, c,
            h, w,
            c*h*w, h*w, w, 1) );
#endif
}


template <class value_type>
void readAllocMemcpy(int size, value_type** data_h, value_type** data_d)
{
    *data_h = new value_type[size];

    //readBinaryFile<value_type>(fname, size, *data_h);

    int size_b = size*sizeof(value_type);
    checkCudaErrors( cudaMalloc(data_d, size_b) );
    checkCudaErrors( cudaMemcpy(*data_d, *data_h,
            size_b,
            cudaMemcpyHostToDevice) );
}


typedef enum {
    FP16_HOST  = 0,
    FP16_CUDA  = 1,
    FP16_CUDNN = 2
} fp16Import_t;
template <class value_type>
struct Layer_t
{
    fp16Import_t fp16Import;
    int inputs;
    int outputs;
    // linear dimension (i.e. size is kernel_dim * kernel_dim)
    int kernel_dim_r, kernel_dim_s;
    value_type *data_h, *data_d;
    value_type *bias_h, *bias_d;
    Layer_t() : data_h(NULL), data_d(NULL), bias_h(NULL), bias_d(NULL),
            inputs(0), outputs(0), kernel_dim_r(0), kernel_dim_s(0), fp16Import(FP16_HOST){};
    Layer_t(int _inputs, int _outputs, int _kernel_dim_r, int kernel_dim_s, fp16Import_t _fp16Import = FP16_HOST)
    : inputs(_inputs), outputs(_outputs), kernel_dim_r(_kernel_dim_r), kernel_dim_s(kernel_dim_s)
    {
        fp16Import = _fp16Import;
        readAllocInit(inputs * outputs * kernel_dim_r * kernel_dim_s,
                &data_h, &data_d);
        readAllocInit(outputs, &bias_h, &bias_d);
    }
    ~Layer_t()
    {
        if (data_h != NULL) delete [] data_h;
        if (data_d != NULL) checkCudaErrors( cudaFree(data_d) );
        if (bias_h != NULL) delete [] bias_h;
        if (bias_d != NULL) checkCudaErrors( cudaFree(bias_d) );
    }
private:
    void readAllocInit(int size, value_type** data_h, value_type** data_d)
    {
        readAllocMemcpy<value_type>(size, data_h, data_d);
    }
};

// Need the map, since scaling factor is of float type in half precision
// Also when one needs to use float instead of half, e.g. for printing
template <typename T>
struct ScaleFactorTypeMap { typedef T Type;};
template <> struct ScaleFactorTypeMap<half1>  { typedef float Type;};

template <class value_type>
class network_t
{
    typedef typename ScaleFactorTypeMap<value_type>::Type scaling_type;
    int convAlgorithm;
    cudnnDataType_t dataType;
    cudnnTensorFormat_t tensorFormat;
    cudnnHandle_t cudnnHandle;
    cudnnTensorDescriptor_t srcTensorDesc, dstTensorDesc, biasTensorDesc;
    cudnnFilterDescriptor_t filterDesc;
    cudnnConvolutionDescriptor_t convDesc;
    cudnnPoolingDescriptor_t poolingDesc;
    cudnnLRNDescriptor_t   normDesc;
    cublasHandle_t cublasHandle;
    void createHandles()
    {
        checkCUDNN( cudnnCreate(&cudnnHandle) );
        checkCUDNN( cudnnCreateTensorDescriptor(&srcTensorDesc) );
        checkCUDNN( cudnnCreateTensorDescriptor(&dstTensorDesc) );
        checkCUDNN( cudnnCreateTensorDescriptor(&biasTensorDesc) );
        checkCUDNN( cudnnCreateFilterDescriptor(&filterDesc) );
        checkCUDNN( cudnnCreateConvolutionDescriptor(&convDesc) );
        checkCUDNN( cudnnCreatePoolingDescriptor(&poolingDesc) );
        checkCUDNN( cudnnCreateLRNDescriptor(&normDesc) );

        checkCublasErrors( cublasCreate(&cublasHandle) );
    }
    void destroyHandles()
    {
        checkCUDNN( cudnnDestroyLRNDescriptor(normDesc) );
        checkCUDNN( cudnnDestroyPoolingDescriptor(poolingDesc) );
        checkCUDNN( cudnnDestroyConvolutionDescriptor(convDesc) );
        checkCUDNN( cudnnDestroyFilterDescriptor(filterDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(srcTensorDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(dstTensorDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(biasTensorDesc) );
        checkCUDNN( cudnnDestroy(cudnnHandle) );

        checkCublasErrors( cublasDestroy(cublasHandle) );
    }
public:
    network_t()
{
        convAlgorithm = -1;
        switch (sizeof(value_type))
        {
        case 2 : dataType = CUDNN_DATA_HALF; break;
        case 4 : dataType = CUDNN_DATA_FLOAT; break;
        case 8 : dataType = CUDNN_DATA_DOUBLE; break;
        default : FatalError("Unsupported data type");
        }
        tensorFormat = CUDNN_TENSOR_NCHW;
        createHandles();
};
    ~network_t()
    {
        destroyHandles();
    }
    void resize(int size, value_type **data)
    {
        if (*data != NULL)
        {
            checkCudaErrors( cudaFree(*data) );
        }
        checkCudaErrors( cudaMalloc(data, size*sizeof(value_type)) );
    }
    void setConvolutionAlgorithm(const cudnnConvolutionFwdAlgo_t& algo)
    {
        convAlgorithm = (int) algo;
    }
    void addBias(const cudnnTensorDescriptor_t& dstTensorDesc, const Layer_t<value_type>& layer, int c, value_type *data)
    {
        setTensorDesc(biasTensorDesc, tensorFormat, dataType, 1, c, 1, 1);

        scaling_type alpha = scaling_type(1);
        scaling_type beta  = scaling_type(1);

        checkCUDNN( cudnnAddTensor(cudnnHandle,
                &alpha, biasTensorDesc,
                layer.bias_d,
                &beta,
                dstTensorDesc,
                data) );
    }

    void allocateDeviceMatrices(int m, int n, int k, float*& d_A, float*& d_B,
            float*& d_C) {
        /* Allocate device memory for the matrices */
        std::cout << "Allocating device matrices" << std::endl;
        printf("Allocating [%d by %d], [%d by %d], and [%d by %d].\n", m, k, k, n,
                m, n);
        checkCudaErrors(cudaMalloc((void ** ) (&d_A), m * k * sizeof(d_A[0])));
        checkCudaErrors(cudaMalloc((void ** ) (&d_B), k * n * sizeof(d_B[0])));
        checkCudaErrors(cudaMalloc((void ** ) (&d_C), m * n * sizeof(d_C[0])));
    }

    void callCuBlasSgemm(cublasHandle_t cublasHandle, cublasOperation_t transA, cublasOperation_t transB, int m, int n, int k, float alpha,
            float beta, float* d_A, float* d_B, float* d_C) {
        printf("Multiplying [%d by %d] times [%d by %d].\n", m, k, k, n);

        cublasSgemm_v2(cublasHandle, transA, transB, m, n, k, &alpha, d_A, m, d_B, k, &beta, d_C, m);
    }


    void applyFeedforward(int batchSize, int outputNeurons, int inputNeurons, int timeSteps) {
        float *d_A, *d_B, *d_C, *d_A1, *d_B1, *d_C1;
        cublasOperation_t transA = CUBLAS_OP_N, transB = CUBLAS_OP_N;

        int m = batchSize * timeSteps, n = outputNeurons, k = inputNeurons;
        allocateDeviceMatrices(m, n, k, d_A, d_B, d_C);

        callCuBlasSgemm(cublasHandle, transA, transB, m, n, k, 1.0, 0.0, d_A, d_B, d_C);
    }

    void applyGRULayer(int depth, int batchSize, int outputNeurons, int inputNeurons, int timeSteps) {

        float *d_A, *d_B, *d_C, *d_A1, *d_B1, *d_C1;
        cublasOperation_t transA = CUBLAS_OP_N, transB = CUBLAS_OP_N;

        int m = batchSize, n = outputNeurons * 3, k = inputNeurons;

        allocateDeviceMatrices(m, n, k, d_A, d_B, d_C);

        int m1 = batchSize, n1 = outputNeurons * 3, k1 = outputNeurons;
        allocateDeviceMatrices(m1, n1, k1, d_A1, d_B1, d_C1);

        for (int i = 0 ; i < timeSteps; i++) {
            callCuBlasSgemm(cublasHandle, transA, transB, m, n, k, 1.0, 0.0, d_A, d_B, d_C);
            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);

            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);
            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);

            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);
            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);

            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);
            callCuBlasSgemm(cublasHandle, transA, transB, m1, n1, k1, 1.0, 0.0, d_A, d_B, d_C);
        }
    }

    void applyReluActivation(int n, int c, int h, int w, cudnnTensorDescriptor_t xDesc, value_type* xData, value_type** yData) {
        cudnnActivationDescriptor_t activationDesc;
        cudnnNanPropagation_t reluNanOpt = CUDNN_PROPAGATE_NAN;
        double reluCeiling = 1.0;
        cudnnActivationMode_t mode;

        scaling_type alpha = scaling_type(1);
        scaling_type beta  = scaling_type(0);

        resize(n*c*h*w, yData);

        checkCUDNN( cudnnCreateActivationDescriptor(&activationDesc));
        checkCUDNN( cudnnSetActivationDescriptor(activationDesc, CUDNN_ACTIVATION_RELU, reluNanOpt, reluCeiling));
        checkCUDNN( cudnnActivationForward_v4(cudnnHandle,
                activationDesc,
                &alpha,
                xDesc,
                xData,
                &beta,
                dstTensorDesc,
                *yData));
    }

    void convoluteForward(const Layer_t<value_type>& conv,
            int& n, int& c, int& h, int& w,
            value_type* srcData, value_type** dstData,
            int stride_u, int stride_v, int pad_h, int pad_w)
    {
        cudnnConvolutionFwdAlgo_t algo;

        setTensorDesc(srcTensorDesc, tensorFormat, dataType, n, c, h, w);

        std::cout << "Input image dimension = " << n << "\t" << c << "\t" << h << "\t" << w << "\n";

        const int tensorDims = 4;
        int tensorOuputDimA[tensorDims] = {n,c,h,w};
        const int filterDimA[tensorDims] = {conv.outputs, conv.inputs,
                conv.kernel_dim_r, conv.kernel_dim_s};
        std::cout << "Filter dimension = " << conv.outputs << "\t" << conv.inputs << "\t" << conv.kernel_dim_r << "\t" << conv.kernel_dim_s << "\n";


        checkCUDNN( cudnnSetFilterNdDescriptor(filterDesc,
                dataType,
                CUDNN_TENSOR_NCHW,
                tensorDims,
                filterDimA) );

        const int convDims = 2;
        int padA[convDims] = {pad_h, pad_w};
        int filterStrideA[convDims] = {stride_u, stride_v};  //was 1
        int upscaleA[convDims] = {1,1};
        checkCUDNN( cudnnSetConvolutionNdDescriptor(convDesc,
                convDims,
                padA,
                filterStrideA,
                upscaleA,
                CUDNN_CROSS_CORRELATION
                //                                                    ));
                ,CUDNN_DATA_FLOAT) );  //TODO: for changing cudnn versions
        // find dimension of convolution output
        checkCUDNN( cudnnGetConvolutionNdForwardOutputDim(convDesc,
                srcTensorDesc,
                filterDesc,
                tensorDims,
                tensorOuputDimA) );
        n = tensorOuputDimA[0]; c = tensorOuputDimA[1];
        h = tensorOuputDimA[2]; w = tensorOuputDimA[3];

        std::cout << "Convolution output dimension = " << n << "\t" << c << "\t" << h << "\t" << w << "\n";

        setTensorDesc(dstTensorDesc, tensorFormat, dataType, n, c, h, w);

        algo = (cudnnConvolutionFwdAlgo_t)convAlgorithm;

        resize(n*c*h*w, dstData);
        size_t sizeInBytes=0;
        void* workSpace=NULL;
        checkCUDNN( cudnnGetConvolutionForwardWorkspaceSize(cudnnHandle,
                srcTensorDesc,
                filterDesc,
                convDesc,
                dstTensorDesc,
                algo,
                &sizeInBytes) );
        if (sizeInBytes!=0)
        {
            checkCudaErrors( cudaMalloc(&workSpace,sizeInBytes) );
        }
        scaling_type alpha = scaling_type(1);
        scaling_type beta  = scaling_type(0);
        checkCUDNN( cudnnConvolutionForward(cudnnHandle,
                &alpha,
                srcTensorDesc,
                srcData,
                filterDesc,
                conv.data_d,
                convDesc,
                algo,
                workSpace,
                sizeInBytes,
                &beta,
                dstTensorDesc,
                *dstData) );
        addBias(dstTensorDesc, conv, c, *dstData);
        if (sizeInBytes!=0)
        {
            checkCudaErrors( cudaFree(workSpace) );
        }
    }

    int runConvolutionLayer(const Layer_t<value_type>& conv1, int n, int c, int h, int w, int stride_u, int stride_v, int pad_h, int pad_w)
    {
        value_type *srcData = NULL, *dstData = NULL, *dstData2 = NULL, *dstData3;
        //value_type imgData_h[n*c*h*w];
        value_type *imgData_h = new value_type[n*c*h*w];

        checkCudaErrors( cudaMalloc(&srcData, n*c*h*w*sizeof(value_type)) );
        checkCudaErrors( cudaMemcpy(srcData, imgData_h,
                n * c * h * w * sizeof(value_type),
                cudaMemcpyHostToDevice) );
        convoluteForward(conv1, n, c, h, w, srcData, &dstData, stride_u, stride_v, pad_h, pad_w);
        applyReluActivation(n, c, h, w, dstTensorDesc, dstData, &dstData2);

//        int nDimsRequested= 4, dimA[4], nDims, strideA[4];
//        checkCUDNN(cudnnGetTensorNdDescriptor(dstTensorDesc,
//                                        nDimsRequested,
//                                        &dataType,
//                                        &nDims,
//                                        dimA,
//                                        strideA));
//
//        printf("Output dimensions = [%d, %d, %d, %d].\n", dimA[0], dimA[1], dimA[2], dimA[3]);

        return 0;
    }
};



int main(int argc, char *argv[])
{

    int n, inputChannels, h, w, outputChannels, kernel_dim_r, kernel_dim_s, stride_u, stride_v, pad_h, pad_w;

    cudnnConvolutionFwdAlgo_t algos[] = {    CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM,
            CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM,
            CUDNN_CONVOLUTION_FWD_ALGO_GEMM};

    std::string algoNames[] = {    "CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM",
            "CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM",
            "CUDNN_CONVOLUTION_FWD_ALGO_GEMM"};
    int version = (int)cudnnGetVersion();
    printf("cudnnGetVersion() : %d , CUDNN_VERSION from cudnn.h : %d (%s)\n", version, CUDNN_VERSION, CUDNN_VERSION_STR);
    printf("Host compiler version : %s %s\r", COMPILER_NAME, COMPILER_VER);
    showDevices();

    for (int i = 0; i < 3; i++) {

        network_t<float> fullPrecisionNetwork;
        fullPrecisionNetwork.setConvolutionAlgorithm(algos[i]);

        n = 1;
        inputChannels = 1;
        outputChannels = 32;
        h = 161;
        w = 700;
        kernel_dim_r = 5;
        kernel_dim_s = 20;
        stride_u = 2;
        stride_v = 2;
        pad_h = pad_w = 0;


        std::cout<<"-----------------------------------------------------------------------------" <<std::endl;
        std::cout << "Testing with algorithm: "<<algoNames[i] <<std::endl;
        std::cout<<"-----------------------------------------------------------------------------" <<std::endl;


        {
            std::cout << "Convolution layer 1" <<std::endl;

            Layer_t<float> conv(inputChannels, outputChannels, kernel_dim_r, kernel_dim_s);
            callBoundary();
            fullPrecisionNetwork.runConvolutionLayer(conv, n, inputChannels, h, w, stride_u, stride_v, pad_h ,pad_w);
            callBoundary();
        }

        n = 1;
        inputChannels = 32;
        outputChannels = 32;
        h = 79;
        w = 341;
        kernel_dim_r = 5;
        kernel_dim_s = 10;
        stride_u = 1;
        stride_v = 2;




        {
            std::cout << "Convolution layer 2" <<std::endl;
            Layer_t<float> conv(inputChannels, outputChannels, kernel_dim_r, kernel_dim_s);
            callBoundary();
            fullPrecisionNetwork.runConvolutionLayer(conv, n, inputChannels, h, w, stride_u, stride_v, pad_h, pad_w);
            callBoundary();

        }

        {
            std::cout << "GRU Layers" <<std::endl;
            //fullPrecisionNetwork.applyGRULayer(4, 1, 2560, 2400, 166);
            fullPrecisionNetwork.applyGRULayer(4, 1, 2560, 2400, 1);
            callBoundary();
        }

        {
            std::cout << "Feedforward layer" <<std::endl;
            fullPrecisionNetwork.applyFeedforward(1, 35, 2560, 1);  //last par is timeSteps done in one go
            callBoundary();
        }

        break;
    }

    cudaDeviceReset();
    exit(EXIT_SUCCESS);

    return 0;
}
