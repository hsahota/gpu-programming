/*
 * constants.h
 *
 *  Created on: May 11, 2016
 *      Author: hsahota
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_


#define BLOCK_SIZE 28
#define KERNEL_SIZE 5
#define IMAGE_SIZE 224

#define CHANNELS 380


#endif /* CONSTANTS_H_ */
