/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda.h>

#include "error_util.h"
#include "constants.h"

//Both kernel and image are in the shared memory

extern __shared__ float s[];

__device__ float dotProductSharedMem(float*kernel) {
    float rVal = 0.0;
    float *sKernel = &s[(blockDim.x+KERNEL_SIZE-1)*(blockDim.x+KERNEL_SIZE-1)];
//    float *sKernel = &s[(8+11-1)*(8+11-1)];

    for (int i = 0; i < KERNEL_SIZE; i++) {
        for (int j = 0; j < KERNEL_SIZE; j++) {
//            assert(i * KERNEL_SIZE + j < KERNEL_SIZE*KERNEL_SIZE);
            rVal += s[(threadIdx.y + i) * blockDim.x + threadIdx.x + j] * sKernel[i * KERNEL_SIZE + j];
        }
    }
    return rVal;
}

__device__ void copyKernelToSharedMemory(float* kernel) {
    float *sKernel = &s[(blockDim.x+KERNEL_SIZE-1)*(blockDim.x+KERNEL_SIZE-1)];
//    float *sKernel = &s[(8+11-1)*(8+11-1)];

    if (threadIdx.x >= KERNEL_SIZE || threadIdx.y >=KERNEL_SIZE){
        return;
    }
    sKernel[threadIdx.y*KERNEL_SIZE + threadIdx.x] = kernel[threadIdx.y*KERNEL_SIZE+threadIdx.x];
}

__device__ void copyImageBlockToSharedMemory(float*image) {

    unsigned int globalX = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int globalY = threadIdx.y + blockIdx.y * blockDim.y;

    if (globalX >= IMAGE_SIZE || globalY >= IMAGE_SIZE) {
        return;
    }

    unsigned int localX = threadIdx.x + (KERNEL_SIZE-1)/2;  //accounting for the margins
    unsigned int localY = threadIdx.y + (KERNEL_SIZE-1)/2;
    unsigned int localStride = blockDim.y + KERNEL_SIZE - 1;


    s[localY * localStride + localX] = image[globalY * IMAGE_SIZE + globalX];

}

/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerZeroes(int values, int which, int localStride) {

    unsigned int localX, localY;
    switch(which) {
        case 0:  //top-left
//            assert(blockIdx.x == 0 || blockIdx.y == 0);
            if (threadIdx.x > 0 || threadIdx.y > 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    s[localY * localStride + localX] = 0.0;
                }
            }
            break;
        case 1:  //top-right
//            assert((blockIdx.x == 0) || (blockIdx.y == (gridDim.y-1)));
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    s[localY * localStride + localX] = 0.0;
                }
            }
            break;
        case 2:  //bottom-right
//            assert((blockIdx.y == (gridDim.y-1)) || (blockIdx.x==(gridDim.x-1)));
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == (blockDim.y-1)) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    s[localY * localStride + localX] = 0.0;
                }
            }
            break;
        case 3:  //bottom-left
//            assert((blockIdx.y == 0) || (blockIdx.x==(gridDim.x-1)));
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    s[localY * localStride + localX] = 0.0;
                }
            }
            break;
        default:
        	;
//            assert(0);
    }
}


/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerFromImage(float *image, int values, int which, int localStride, int globalStride) {
    unsigned int globalXBase, globalYBase;
    unsigned int localX, localY;
    unsigned int globalX, globalY;

    switch(which) {
        case 0:  //top-left
//            assert(blockIdx.x > 0 && blockIdx.y > 0);
            if (threadIdx.x >0 || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y - values;
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localY * localStride + localX] = image[globalY * globalStride + globalX];
                }
            }
            break;
        case 1:  //top-right
//            assert(blockIdx.x > 0 && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localY * localStride + localX] = image[globalY * globalStride + globalX];
                }
            }
            break;
        case 2:  //bottom-right
//            assert((blockIdx.x < gridDim.x-1) && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y < (blockDim.y-1)) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localY * localStride + localX] = image[globalY * globalStride + globalX];
                }
            }
            break;
        case 3:  //bottom-left
//            assert((blockIdx.x < gridDim.x-1) && blockIdx.y >0);
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localY * localStride + localX] = image[globalY * globalStride + globalX];
                }
            }
            break;
        default:
        	;
//            assert(0);
    }
}

__device__ void copyLeftRightMarginZeroes(int values, bool left, int localStride) {
    if (threadIdx.y > 0) {
        return;
    }
    unsigned int localX, localY;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
//        assert(blockIdx.y == 0);

        for (int i = 0; i < values; i++) {
            localY = i;

            s[localY * localStride + localX] = 0.0;
        }
    } else { //right
//        assert(blockIdx.y == (gridDim.y-1));

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle

            s[localY * localStride + localX] = 0.0;
        }
    }
}


__device__ void copyLeftRightMarginFromImage(float *image, int values, bool left, int localStride, int globalStride) {
    if (threadIdx.y > 0) {
        return;
    }
    unsigned int globalYBase, globalY, globalX, localX, localY;
    globalX = threadIdx.x + blockIdx.x*blockDim.x;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
//        assert(blockIdx.y > 0);
        globalYBase = blockIdx.y * blockDim.y - values;

        for (int i = 0; i < values; i++) {
            localY = i;
            globalY = globalYBase + i;

            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }
    } else { //right
//        assert(blockIdx.y < (gridDim.y-1));
        globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for the the image core in the middle

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle
            globalY = globalYBase + i;

            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }
    }
}

__device__ void copyTopBottomMarginZeroes(int values, bool top, int localStride) {
    if (threadIdx.x > 0) {
        return;
    }
    unsigned int localX;
    unsigned int localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
//        assert(blockIdx.x == 0);1


        for (int i = 0; i < values; i++) {
            localX = i;
            s[localX * localStride + localY] = 0.0;
        }

    } else { //bottom
//        assert(blockIdx.x == (gridDim.x-1));

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            s[localY * localStride + localX] = 0.0;
        }
    }
}

__device__ void copyTopBottomMarginFromImage(float *image, int values, bool top, int localStride, int globalStride) {
    if (threadIdx.x > 0) {
        return;
    }
    unsigned int globalXBase, globalX, globalY;
    unsigned int localX, localY;
    globalY = threadIdx.y + blockIdx.y*blockDim.y;
    localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
//        assert(blockIdx.x > 0);

        globalXBase = blockIdx.x * blockDim.x - values;

        for (int i = 0; i < values; i++) {
            localX = i;


            globalX = globalXBase + i;
            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }

    } else { //bottom
//        assert(blockIdx.x < (gridDim.x-1));
        globalXBase = blockIdx.x * blockDim.x + blockDim.x;  //account for the image core in the middle

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            globalX = globalXBase + i;
            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }
    }
}



__device__ void copyImageWithPadddingToSharedMemory(float* image) {
    copyImageBlockToSharedMemory(image);
    unsigned int values = (KERNEL_SIZE - 1) / 2;
    unsigned int localStride = blockDim.x + KERNEL_SIZE - 1; //Make sure that the thread blocks are square
    unsigned int globalStride = IMAGE_SIZE;
    if (blockIdx.x == 0) {
        copyMarginCornerZeroes(values, 0, localStride); //top left
        copyMarginCornerZeroes(values, 1, localStride); //top right
        copyTopBottomMarginZeroes(values, true, localStride); //top
        copyTopBottomMarginFromImage(image, values, false, localStride,
                globalStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 3, localStride); //bottom left
            copyLeftRightMarginZeroes(values, true, localStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 2, localStride); //bottom right
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
        } else {
            //copy both bottom corners from image
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
        }
    } else if (blockIdx.x == gridDim.x - 1) {
        copyTopBottomMarginFromImage(image, values, true, localStride,
                globalStride);
        copyTopBottomMarginZeroes(values, false, localStride);
        copyMarginCornerZeroes(values, 2, localStride);
        copyMarginCornerZeroes(values, 3, localStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 0, localStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, true, localStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 1, localStride);
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
        } else {
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
        }
    } else {
        copyTopBottomMarginFromImage(image, values, true, localStride,
                globalStride);
        copyTopBottomMarginFromImage(image, values, false, localStride,
                globalStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 0, localStride);
            copyMarginCornerZeroes(values, 3, localStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, true, localStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 1, localStride);
            copyMarginCornerZeroes(values, 2, localStride);
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
        } else {
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
        }
    }
}

__global__ void convoluteMultiThreadedSharedMemory(float* image, float *kernel, float* output) {
    copyImageWithPadddingToSharedMemory(image);
    copyKernelToSharedMemory(kernel);

    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    int idy = threadIdx.y + blockIdx.y*blockDim.y;
    __syncthreads();
//    assert(idx*IMAGE_SIZE+idy < IMAGE_SIZE*IMAGE_SIZE);
    output[idy * IMAGE_SIZE + idx] = dotProductSharedMem(kernel);
}



void fill2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                h_Data[i * w + j] = i * w + j;
            }
        }
}

void show2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                std::cout << h_Data[i * w + j] << "\t";
            }
            std::cout << std::endl;
        }
}


int main(int argc, char *argv[])
{
    float *h_Image, *h_Kernel, *d_Image, *d_Kernel, *d_Output;

    h_Image = (float*)malloc(227 * 227 * sizeof(float));
    h_Kernel = (float*)malloc(11 * 11 * sizeof(float));


    fill2D(h_Image, IMAGE_SIZE, IMAGE_SIZE);
    fill2D(h_Kernel, KERNEL_SIZE, KERNEL_SIZE);

//    std::cout << "Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);

    checkCudaErrors(cudaMalloc(&d_Image,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Output,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Kernel,KERNEL_SIZE*KERNEL_SIZE*sizeof(float)));

    checkCudaErrors(cudaMemcpy(d_Image, h_Image, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_Kernel, h_Kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), cudaMemcpyHostToDevice));


    dim3 gridDim(IMAGE_SIZE/BLOCK_SIZE, IMAGE_SIZE/BLOCK_SIZE, 1);
    dim3 blockDim(BLOCK_SIZE, BLOCK_SIZE, 1);
    std::size_t sharedMemSize = ((blockDim.x + (KERNEL_SIZE - 1))
            * (blockDim.x + (KERNEL_SIZE - 1)) + KERNEL_SIZE*KERNEL_SIZE + 3) * sizeof(float);


    convoluteMultiThreadedSharedMemory<<<gridDim, blockDim, sharedMemSize>>>(
            d_Image, d_Kernel, d_Output);


    checkCudaErrors(cudaMemcpy(h_Image, d_Output, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaMemcpy(h_Kernel, d_Kernel, 227 * 227 * sizeof(float), cudaMemcpyDeviceToHost));

//    std::cout << "After Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "After Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);


    return 0;
}


