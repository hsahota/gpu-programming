/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda.h>

#include "error_util.h"
#include "constants.h"

//Both kernel and image are in the shared memory; block size covers the margin pixels also


extern __shared__ float s[];

__device__ float dotProductSharedMem(float*kernel) {
    float rVal = 0.0;
    float *sKernel = &s[(blockDim.x)*(blockDim.x)];
//    float *sKernel = &s[(8+11-1)*(8+11-1)];

    for (int i = 0; i < KERNEL_SIZE; i++) {
        for (int j = 0; j < KERNEL_SIZE; j++) {
//            assert(i * KERNEL_SIZE + j < KERNEL_SIZE*KERNEL_SIZE);
//        	assert((threadIdx.x + i) * blockDim.y + threadIdx.y + j < blockDim.y*blockDim.x);
        	float k = sKernel[i * KERNEL_SIZE + j];
//        	float k = 0.0;
        	float im = s[(threadIdx.y + i) * blockDim.x + threadIdx.x + j];
//        	float im = 0.0;
//            rVal += s[(threadIdx.x + i) * IMAGE_SIZE + threadIdx.y + j] * sKernel[i * KERNEL_SIZE + j];
            rVal += k * im;
        }
    }
    return rVal;
}

__device__ void copyKernelToSharedMemory(float* kernel) {
    float *sKernel = &s[(blockDim.x)*(blockDim.x)];
//    float *sKernel = &s[(8+11-1)*(8+11-1)];

    if (threadIdx.x >= KERNEL_SIZE || threadIdx.y >=KERNEL_SIZE){
        return;
    }
    sKernel[threadIdx.y*KERNEL_SIZE + threadIdx.x] = kernel[threadIdx.y*KERNEL_SIZE+threadIdx.x];
}

__device__ void copyImageBlockToSharedMemory(float*image) {

    int globalX = threadIdx.x + blockIdx.x * blockDim.x - (KERNEL_SIZE -1)/2;
    int globalY = threadIdx.y + blockIdx.y * blockDim.y - (KERNEL_SIZE - 1)/2;

    if ((globalX >= (IMAGE_SIZE + KERNEL_SIZE -1)) || (globalY >= (IMAGE_SIZE + KERNEL_SIZE -1))) {
        return;
    }

    unsigned int localX = threadIdx.x;
    unsigned int localY = threadIdx.y;
    unsigned int localStride = blockDim.y;

//    assert(localX < (8 + KERNEL_SIZE-1));
//    assert(localY < (8 + KERNEL_SIZE-1));

    if (globalX < 0 || globalX >= IMAGE_SIZE || globalY < 0 || globalY >= IMAGE_SIZE) {
    	s[localY * localStride + localX] = 0.0;
    } else {
    	s[localY * localStride + localX] = image[globalY * IMAGE_SIZE + globalX];
    }

}




__device__ void copyImageWithPadddingToSharedMemory(float* image) {
    copyImageBlockToSharedMemory(image);
}

__global__ void convoluteMultiThreadedSharedMemory(float* image, float *kernel, float* output) {
    copyImageWithPadddingToSharedMemory(image);
    copyKernelToSharedMemory(kernel);

    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    int idy = threadIdx.y + blockIdx.y*blockDim.y;
    __syncthreads();
    if (threadIdx.x >= BLOCK_SIZE || threadIdx.y >= BLOCK_SIZE){ //TODO: Image tile size
    	return;
    }
    output[idy * IMAGE_SIZE + idx] = dotProductSharedMem(kernel);
}



void fill2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                h_Data[i * w + j] = i * w + j;
            }
        }
}

void show2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                std::cout << h_Data[i * w + j] << "\t";
            }
            std::cout << std::endl;
        }
}


int main(int argc, char *argv[])
{
    float *h_Image, *h_Kernel, *d_Image, *d_Kernel, *d_Output;

    h_Image = (float*)malloc(IMAGE_SIZE * IMAGE_SIZE * sizeof(float));
    h_Kernel = (float*)malloc(KERNEL_SIZE * KERNEL_SIZE * sizeof(float));


    fill2D(h_Image, IMAGE_SIZE, IMAGE_SIZE);
    fill2D(h_Kernel, KERNEL_SIZE, KERNEL_SIZE);

//    std::cout << "Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);

    checkCudaErrors(cudaMalloc(&d_Image,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Output,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Kernel,KERNEL_SIZE*KERNEL_SIZE*sizeof(float)));

    checkCudaErrors(cudaMemcpy(d_Image, h_Image, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_Kernel, h_Kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), cudaMemcpyHostToDevice));


    dim3 gridDim(IMAGE_SIZE/BLOCK_SIZE, IMAGE_SIZE/BLOCK_SIZE, 1);
    dim3 blockDim(BLOCK_SIZE + KERNEL_SIZE-1, BLOCK_SIZE + KERNEL_SIZE-1, 1);
    std::size_t sharedMemSize = (blockDim.x * blockDim.x + KERNEL_SIZE*KERNEL_SIZE) * sizeof(float);


    convoluteMultiThreadedSharedMemory<<<gridDim, blockDim, sharedMemSize>>>(
            d_Image, d_Kernel, d_Output);

    checkCudaErrors(cudaGetLastError());


    checkCudaErrors(cudaMemcpy(h_Image, d_Output, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaMemcpy(h_Kernel, d_Kernel, 227 * 227 * sizeof(float), cudaMemcpyDeviceToHost));

//    std::cout << "After Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "After Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);


    return 0;
}


