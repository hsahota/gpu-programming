/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda.h>

#include "error_util.h"

#include "constants.h"

__constant__ float kernel[KERNEL_SIZE*KERNEL_SIZE];
texture<float, 2, cudaReadModeElementType> texImage;
surface<void, 2> surfImage;  //output

__device__ float dotProductSharedMem() {
    float rVal = 0.0;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

    for (int i = 0; i < KERNEL_SIZE; i++) {
        for (int j = 0; j < KERNEL_SIZE; j++) {
            rVal += tex2D(texImage, x + i - KERNEL_SIZE/2, y + j - KERNEL_SIZE/2) * kernel[i * KERNEL_SIZE + j];
        }
    }
    return rVal;
}


__global__ void convoluteMultiThreadedSharedMemory(float* output) {
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    int idy = threadIdx.y + blockIdx.y*blockDim.y;
//    assert(idx*IMAGE_SIZE+idy < IMAGE_SIZE*IMAGE_SIZE);

    output[idy * IMAGE_SIZE + idx] = dotProductSharedMem();
}

__global__ void convoluteMultiThreadedTextureMemory() {
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    int idy = threadIdx.y + blockIdx.y*blockDim.y;
//    assert(idx < IMAGE_SIZE);
//    assert(idy < IMAGE_SIZE);
//    assert(idx*IMAGE_SIZE+idy < IMAGE_SIZE*IMAGE_SIZE);

    float data = dotProductSharedMem();
    surf2Dwrite(data, surfImage, idx * 4, idy * 4, cudaBoundaryModeTrap);
}



void fill2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                h_Data[i * w + j] = i * w + j;
            }
        }
}

void show2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                std::cout << h_Data[i * w + j] << "\t";
            }
            std::cout << std::endl;
        }
}


int main(int argc, char *argv[])
{
    float *h_image, *h_Kernel, *d_Output;

    h_image = (float*)malloc(IMAGE_SIZE * IMAGE_SIZE * sizeof(float));
    h_Kernel = (float*)malloc(KERNEL_SIZE * KERNEL_SIZE * sizeof(float));

    fill2D(h_image, IMAGE_SIZE, IMAGE_SIZE);
    fill2D(h_Kernel, KERNEL_SIZE, KERNEL_SIZE);

    size_t size = IMAGE_SIZE * IMAGE_SIZE * sizeof(float);

    checkCudaErrors(cudaMalloc(&d_Output,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(kernel, h_Kernel, KERNEL_SIZE*KERNEL_SIZE*sizeof(float), 0, cudaMemcpyHostToDevice));

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    cudaChannelFormatDesc outputChannelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    cudaArray *cuArray, *cuOutputArray;

    checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, IMAGE_SIZE, IMAGE_SIZE));
    checkCudaErrors(cudaMemcpyToArray(cuArray, 0, 0, h_image, size, cudaMemcpyHostToDevice));

    texImage.addressMode[0] = cudaAddressModeBorder;
    texImage.addressMode[1] = cudaAddressModeBorder;
    texImage.filterMode = cudaFilterModePoint;
    texImage.normalized = false;

    checkCudaErrors(cudaBindTextureToArray(texImage, cuArray, channelDesc));



    dim3 gridDim(IMAGE_SIZE/BLOCK_SIZE, IMAGE_SIZE/BLOCK_SIZE, 1);
    dim3 blockDim(BLOCK_SIZE, BLOCK_SIZE, 1);


    cudaMallocArray(&cuOutputArray, &outputChannelDesc, IMAGE_SIZE* IMAGE_SIZE, 0, cudaArraySurfaceLoadStore);
    cudaBindSurfaceToArray(surfImage, cuOutputArray);

//    struct cudaResourceDesc resDesc;
//    memset(&resDesc, 0, sizeof(resDesc));
//    resDesc.resType = cudaResourceTypeArray;
//
//    resDesc.res.array.array = cuOutputArray;
//
//    cudaSurfaceObject_t outputSurfObj = 0;
//    cudaCreateSurfaceObject(&outputSurfObj, &resDesc);


    //convoluteMultiThreadedSharedMemory<<<gridDim, blockDim, (blockDim.x + (KERNEL_SIZE-1))* (blockDim.x + (KERNEL_SIZE-1)) * sizeof(float)>>>(d_Image, d_Output, IMAGE_SIZE, KERNEL_SIZE);
    for (int i = 0; i < 1; i++) {
    	convoluteMultiThreadedSharedMemory<<<gridDim, blockDim>>>(d_Output);
    }

    checkCudaErrors(cudaMemcpyFromArray(h_image, cuArray, 0, 0, size, cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaDestroySurfaceObject(outputSurfObj));
    return 0;
}


