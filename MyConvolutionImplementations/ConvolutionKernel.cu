/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda.h>

#include "error_util.h"

__device__ float dotProduct(int imageI, int imageJ, float* image, float* kernel, int imageDim, int kernelDim) {
    float rVal = 0.0;
    int iKernelStart = (kernelDim - 1) /2 - imageI > 0 ? (kernelDim - 1) /2 - imageI: 0;
    int jKernelStart = (kernelDim - 1) /2 - imageJ > 0 ? (kernelDim - 1) /2 - imageJ: 0;

    int iKernelStop = (kernelDim - 1) /2 - (imageDim-1-imageI) > 0 ? (kernelDim - 1)/2 + (imageDim - 1 - imageI) : kernelDim;
    int jKernelStop = (kernelDim - 1) /2 - (imageDim-1-imageJ) > 0 ? (kernelDim - 1)/2 + (imageDim - 1 - imageJ) : kernelDim;

    for (int i = iKernelStart, ii = 0; i <= iKernelStop; i++, ii++) {
        for (int j = jKernelStart, jj = 0; j <= jKernelStop; j++, jj++) {
            rVal += image[(imageI + (i - (kernelDim-1) / 2)) * imageDim + imageJ+ (j - (kernelDim - 1) / 2)] * kernel[i * kernelDim + j];
        }
    }
    return rVal;
}

__device__ float dotProductSharedMem() {
    float rVal = 0.0;
    return rVal;
}

__device__ void copyImageBlockToSharedMemory(float*image, int imageDim, int kernelDim) {
    extern __shared__ float s[];
    unsigned int globalX = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int globalY = threadIdx.y + blockIdx.y * blockDim.y;

    if (globalX >= imageDim || globalY >= imageDim) {
        return;
    }

    unsigned int localX = threadIdx.x + (kernelDim-1)/2;  //accounting for the margins
    unsigned int localY = threadIdx.y + (kernelDim-1)/2;
    unsigned int localStride = blockDim.y + kernelDim - 1;


    s[localX * localStride + localY] = image[globalX * imageDim + globalY];
    __syncthreads();
}

/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerZeroes(int values, int which, int localStride) {

    unsigned int localX, localY;
    extern __shared__ float s[];
    switch(which) {
        case 0:  //top-left
            assert(blockIdx.x == 0);
            if (threadIdx.x > 0 || threadIdx.y > 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 1:  //top-right
            assert(blockIdx.x == 0);
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 2:  //bottom-right
            assert(blockIdx.y == (gridDim.y-1));
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == (blockDim.y-1)) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 3:  //bottom-left
            assert(blockIdx.y == 0);
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        default:
            assert(0);
    }
}


/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerFromImage(float *image, int values, int which, int localStride, int globalStride) {
    extern __shared__ float s[];
    unsigned int globalXBase, globalYBase;
    unsigned int localX, localY;
    unsigned int globalX, globalY;

    switch(which) {
        case 0:  //top-left
            assert(blockIdx.x > 0 && blockIdx.y > 0);
            if (threadIdx.x >0 || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y - values;
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 1:  //top-right
            assert(blockIdx.x > 0 && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 2:  //bottom-right
            assert((blockIdx.x < gridDim.x-1) && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y < (blockDim.y-1)) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 3:  //bottom-left
            assert((blockIdx.x < gridDim.x-1) && blockIdx.y >0);
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        default:
            assert(0);
    }
}

__device__ void copyLeftRightMarginZeroes(int values, bool left, int localStride) {
    if (threadIdx.y > 0) {
        return;
    }
    extern __shared__ float s[];
    unsigned int localX, localY;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
        assert(blockIdx.y == 0);

        for (int i = 0; i < values; i++) {
            localY = i;

            s[localX * localStride + localY] = 0.0;
        }
    } else { //right
        assert(blockIdx.y == (gridDim.y-1));

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle

            s[localX * localStride + localY] = 0.0;
        }
    }
}


__device__ void copyLeftRightMarginFromImage(float *image, int values, bool left, int localStride, int globalStride) {
    if (threadIdx.y > 0) {
        return;
    }
    extern __shared__ float s[];
    unsigned int globalYBase, globalY, globalX, localX, localY;
    globalX = threadIdx.x + blockIdx.x*blockDim.x;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
        assert(blockIdx.y > 0);
        globalYBase = blockIdx.y * blockDim.y - values;

        for (int i = 0; i < values; i++) {
            localY = i;
            globalY = globalYBase + i;

            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }
    } else { //right
        assert(blockIdx.y < (gridDim.y-1));
        globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for the the image core in the middle

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle
            globalY = globalYBase + i;

            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }
    }
}

__device__ void copyTopBottomMarginZeroes(int values, bool top, int localStride) {
    if (threadIdx.x > 0) {
        return;
    }
    extern __shared__ float s[];
    unsigned int localX;
    unsigned int localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
        assert(blockIdx.x == 0);


        for (int i = 0; i < values; i++) {
            localX = i;
            s[localX * localStride + localY] = 0.0;
        }

    } else { //bottom
        assert(blockIdx.x == (gridDim.x-1));

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            s[localX * localStride + localY] = 0.0;
        }
    }
}

__device__ void copyTopBottomMarginFromImage(float *image, int values, bool top, int localStride, int globalStride) {
    if (threadIdx.x > 0) {
        return;
    }
    extern __shared__ float s[];
    unsigned int globalXBase, globalX, globalY;
    unsigned int localX, localY;
    globalY = threadIdx.y + blockIdx.y*blockDim.y;
    localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
        assert(blockIdx.x > 0);

        globalXBase = blockIdx.x * blockDim.x - values;

        for (int i = 0; i < values; i++) {
            localX = i;


            globalX = globalXBase + i;
            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }

    } else { //bottom
        assert(blockIdx.x < (gridDim.x-1));
        globalXBase = blockIdx.x * blockDim.x + blockDim.x;  //account for the image core in the middle

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            globalX = globalXBase + i;
            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }
    }
}


__global__ void convoluteMultiThreadedSharedMemory(float* image, float *kernel, float* output, int imageDim, int kernelDim) {
    extern __shared__ float s[];
    copyImageBlockToSharedMemory(image, imageDim, kernelDim);
    unsigned int values = (kernelDim-1)/2;
    unsigned int localStride = blockDim.x + kernelDim-1;  //Make sure that the thread blocks are square
    unsigned int globalStride = imageDim;
    if (blockIdx.x ==0) {
        copyMarginCornerZeroes(values, 0, localStride);  //top left
        copyMarginCornerZeroes(values, 1, localStride);  //top right
        copyTopBottomMarginZeroes(values, true, localStride); //top
        copyTopBottomMarginFromImage(image, values, false, localStride, globalStride);

        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 3, localStride);  //bottom left
            copyLeftRightMarginZeroes(values, true, localStride);
            copyLeftRightMarginFromImage(image, values, false, localStride, globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride, globalStride);

        } else if (blockIdx.y == gridDim.y-1) {
            copyMarginCornerZeroes(values, 2, localStride);  //bottom right
            copyMarginCornerFromImage(image, values, 3, localStride, globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
            copyLeftRightMarginFromImage(image, values,true, localStride, globalStride);

        } else {
            //copy both bottom corners from image
            copyLeftRightMarginFromImage(image, values,true, localStride, globalStride);
            copyLeftRightMarginFromImage(image, values,false, localStride, globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride, globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride, globalStride);
        }
    } else if (blockIdx.x == gridDim.x-1){
        copyTopBottomMarginFromImage(image, values, true, localStride, globalStride);
    } else {

    }
}


__global__ void convoluteMultiThreaded(float* image, float *kernel, float* output, int imageDim, int kernelDim) {
    unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int idy = threadIdx.y + blockIdx.y * blockDim.y;

    if (idx >= imageDim || idx >= imageDim) {
        return;
    }

    assert(idx*imageDim+idy < imageDim*imageDim);

    output[idx * imageDim + idy] = dotProduct(idx , idy, image, kernel, imageDim, kernelDim);
}



__global__ void convolute(float* image, float *kernel, float* output, int imageDim, int kernelDim) {

    for (int i = 0; i < imageDim; i++) {
        for (int j = 0; j < imageDim; j++) {
            output[i * imageDim + j] = dotProduct(i , j, image, kernel, imageDim, kernelDim);
        }
    }
}


void fill2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                h_Data[i * w + j] = i * w + j;
            }
        }
}

void show2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                std::cout << h_Data[i * w + j] << "\t";
            }
            std::cout << std::endl;
        }
}


int main(int argc, char *argv[])
{
    float *h_Image, *h_Kernel, *d_Image, *d_Kernel, *d_Output;

    h_Image = (float*)malloc(227 * 227 * sizeof(float));
    h_Kernel = (float*)malloc(11 * 11 * sizeof(float));


    fill2D(h_Image, 227, 227);
    fill2D(h_Kernel, 11, 11);

//    std::cout << "Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);

    checkCudaErrors(cudaMalloc(&d_Image,227*227*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Output,227*227*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Kernel,227*227*sizeof(float)));

    checkCudaErrors(cudaMemcpy(d_Image, h_Image, 227 * 227 * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_Kernel, h_Kernel, 227 * 227 * sizeof(float), cudaMemcpyHostToDevice));


    dim3 blockDim(32, 32, 1);
    dim3 gridDim(8, 8, 1);
    convolute<<<1,1>>>(d_Image, d_Kernel, d_Output, 227, 11);
    convoluteMultiThreaded<<<gridDim, blockDim>>>(d_Image, d_Kernel, d_Output, 227, 11);
    convoluteMultiThreaded<<<dim3(16, 16, 1), dim3(16, 16, 1)>>>(d_Image, d_Kernel, d_Output, 227, 11);
    convoluteMultiThreaded<<<dim3(32, 32, 1), dim3(8, 8, 1)>>>(d_Image, d_Kernel, d_Output, 227, 11);
    convoluteMultiThreaded<<<dim3(48, 48, 1), dim3(6, 6, 1)>>>(d_Image, d_Kernel, d_Output, 227, 11);

    convoluteMultiThreadedSharedMemory<<<dim3(32, 32, 1), dim3(8,8,1), (8 + 10) * (8 + 10) * sizeof(float)>>>(d_Image, d_Kernel, d_Output, 227, 11);


    checkCudaErrors(cudaMemcpy(h_Image, d_Output, 227 * 227 * sizeof(float), cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaMemcpy(h_Kernel, d_Kernel, 227 * 227 * sizeof(float), cudaMemcpyDeviceToHost));

//    std::cout << "After Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "After Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);


    return 0;
}


