/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda.h>

#include "error_util.h"

#define BLOCK_SIZE 8
#define KERNEL_SIZE 11
#define IMAGE_SIZE 224

__constant__ float kernel[KERNEL_SIZE*KERNEL_SIZE];
__shared__ float s[(BLOCK_SIZE*4+KERNEL_SIZE-1)*(BLOCK_SIZE*4+KERNEL_SIZE-1)];

__device__ float dotProductSharedMem(int ii, int jj) {
//    extern __shared__ float s[];
    float rVal = 0.0;

    for (int i = 0; i < KERNEL_SIZE; i++) {
        for (int j = 0; j < KERNEL_SIZE; j++) {
            rVal += s[(threadIdx.x * 4 + ii + i) * blockDim.y + threadIdx.y * 4 + jj + j] * kernel[i * KERNEL_SIZE + j];
        }
    }
    return rVal;
}

__device__ void copyImageBlockToSharedMemory(float*image) {
//    extern __shared__ float s[];
    unsigned int globalX = threadIdx.x*4 + blockIdx.x * blockDim.x*4;
    unsigned int globalY = threadIdx.y*4 + blockIdx.y * blockDim.y*4;

    if (globalX >= IMAGE_SIZE || globalY >= IMAGE_SIZE) {
        return;
    }

    unsigned int localX = threadIdx.x * 4 + (KERNEL_SIZE-1)/2;  //accounting for the margins
    unsigned int localY = threadIdx.y * 4 + (KERNEL_SIZE-1)/2;
    unsigned int localStride = blockDim.y * 4 + KERNEL_SIZE - 1;

    for (int i = 0; i < 4; i++) {
    	for (int j = 0; j < 4; j++) {
    		s[(localY + i) * localStride + (localX + j)] = image[(globalY+i) * IMAGE_SIZE + (globalX+j)];
    	}
    }
}

/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerZeroes(int values, int which, int localStride) {

    unsigned int localX, localY;
//    extern __shared__ float s[];
    switch(which) {
        case 0:  //top-left
            assert(blockIdx.x == 0 || blockIdx.y == 0);
            if (threadIdx.x > 0 || threadIdx.y > 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 1:  //top-right
            assert((blockIdx.x == 0) || (blockIdx.y == (gridDim.y-1)));
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 2:  //bottom-right
            assert((blockIdx.y == (gridDim.y-1)) || (blockIdx.x==(gridDim.x-1)));
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == (blockDim.y-1)) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        case 3:  //bottom-left
            assert((blockIdx.y == 0) || (blockIdx.x==(gridDim.x-1)));
            if (threadIdx.x == (blockDim.x - 1) || threadIdx.y == 0) {
                return;
            }
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    s[localX * localStride + localY] = 0.0;
                }
            }
            break;
        default:
            assert(0);
    }
}


/**
 * which = 0 for top left, 1 for top right, 2 for bottom right, 3 for bottom left (clock-wise)
 */
__device__ void copyMarginCornerFromImage(float *image, int values, int which, int localStride, int globalStride) {
//    extern __shared__ float s[];
    unsigned int globalXBase, globalYBase;
    unsigned int localX, localY;
    unsigned int globalX, globalY;

    switch(which) {
        case 0:  //top-left
            assert(blockIdx.x > 0 && blockIdx.y > 0);
            if (threadIdx.x >0 || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y - values;
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 1:  //top-right
            assert(blockIdx.x > 0 && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x >0 || threadIdx.y < blockDim.y-1) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x - values;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 2:  //bottom-right
            assert((blockIdx.x < gridDim.x-1) && (blockIdx.y < gridDim.y-1));
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y < (blockDim.y-1)) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j + values + blockDim.y;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        case 3:  //bottom-left
            assert((blockIdx.x < gridDim.x-1) && blockIdx.y >0);
            if (threadIdx.x < (blockDim.x - 1) || threadIdx.y > 0) {
                return;
            }
            globalXBase = blockIdx.x * blockDim.x + blockDim.x;
            globalYBase = blockIdx.y * blockDim.y;  //account for core image in the middle
            for (int i = 0; i < values; i++) {
                for (int j = 0; j < values; j++) {
                    localX = i + values + blockDim.x;
                    localY = j;
                    globalX = globalXBase + i;
                    globalY = globalYBase + j;
                    s[localX * localStride + localY] = image[globalX * globalStride + globalY];
                }
            }
            break;
        default:
            assert(0);
    }
}

__device__ void copyLeftRightMarginZeroes(int values, bool left, int localStride) {
    if (threadIdx.y > 0) {
        return;
    }
//    extern __shared__ float s[];
    unsigned int localX, localY;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
        assert(blockIdx.y == 0);

        for (int i = 0; i < values; i++) {
            localY = i;

            s[localX * localStride + localY] = 0.0;
        }
    } else { //right
        assert(blockIdx.y == (gridDim.y-1));

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle

            s[localX * localStride + localY] = 0.0;
        }
    }
}


__device__ void copyLeftRightMarginFromImage(float *image, int values, bool left, int localStride, int globalStride) {
    if (threadIdx.y > 0) {
        return;
    }
//    extern __shared__ float s[];
    unsigned int globalYBase, globalY, globalX, localX, localY;
    globalX = threadIdx.x + blockIdx.x*blockDim.x;
    localX = threadIdx.x + values; //allow for top margin

    if (left == true) {
        assert(blockIdx.y > 0);
        globalYBase = blockIdx.y * blockDim.y - values;

        for (int i = 0; i < values; i++) {
            localY = i;
            globalY = globalYBase + i;

            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }
    } else { //right
        assert(blockIdx.y < (gridDim.y-1));
        globalYBase = blockIdx.y * blockDim.y + blockDim.y;  //account for the the image core in the middle

        for (int i = 0; i < values; i++) {
            localY = values + blockDim.y + i;     //account for the left margin and the image core in the middle
            globalY = globalYBase + i;

            s[localY * localStride + localX] = image[globalY * globalStride + globalX];
        }
    }
}

__device__ void copyTopBottomMarginZeroes(int values, bool top, int localStride) {
    if (threadIdx.x > 0) {
        return;
    }
//    extern __shared__ float s[];
    unsigned int localX;
    unsigned int localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
        assert(blockIdx.x == 0);


        for (int i = 0; i < values; i++) {
            localX = i;
            s[localX * localStride + localY] = 0.0;
        }

    } else { //bottom
        assert(blockIdx.x == (gridDim.x-1));

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            s[localX * localStride + localY] = 0.0;
        }
    }
}

__device__ void copyTopBottomMarginFromImage(float *image, int values, bool top, int localStride, int globalStride) {
    if (threadIdx.x > 0) {
        return;
    }
//    extern __shared__ float s[];
    unsigned int globalXBase, globalX, globalY;
    unsigned int localX, localY;
    globalY = threadIdx.y + blockIdx.y*blockDim.y;
    localY = threadIdx.y + values; //allow for left margin

    if (top == true) {
        assert(blockIdx.x > 0);

        globalXBase = blockIdx.x * blockDim.x - values;

        for (int i = 0; i < values; i++) {
            localX = i;


            globalX = globalXBase + i;
            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }

    } else { //bottom
        assert(blockIdx.x < (gridDim.x-1));
        globalXBase = blockIdx.x * blockDim.x + blockDim.x;  //account for the image core in the middle

        for (int i = 0; i < values; i++) {
            localX = i + values + blockDim.x;
            globalX = globalXBase + i;
            s[localX * localStride + localY] = image[globalX * globalStride + globalY];
        }
    }
}

__device__ void copyImageWithPadddingToSharedMemory(float* image) {
//    extern __shared__ float s[];
    copyImageBlockToSharedMemory(image);
    unsigned int values = (KERNEL_SIZE - 1) / 2;
    unsigned int localStride = blockDim.x + KERNEL_SIZE - 1; //Make sure that the thread blocks are square
    unsigned int globalStride = IMAGE_SIZE;
    if (blockIdx.x == 0) {
        copyMarginCornerZeroes(values, 0, localStride); //top left
        copyMarginCornerZeroes(values, 1, localStride); //top right
        copyTopBottomMarginZeroes(values, true, localStride); //top
        copyTopBottomMarginFromImage(image, values, false, localStride,
                globalStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 3, localStride); //bottom left
            copyLeftRightMarginZeroes(values, true, localStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 2, localStride); //bottom right
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
        } else {
            //copy both bottom corners from image
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
        }
    } else if (blockIdx.x == gridDim.x - 1) {
        copyTopBottomMarginFromImage(image, values, true, localStride,
                globalStride);
        copyTopBottomMarginZeroes(values, false, localStride);
        copyMarginCornerZeroes(values, 2, localStride);
        copyMarginCornerZeroes(values, 3, localStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 0, localStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, true, localStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 1, localStride);
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
        } else {
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
        }
    } else {
        copyTopBottomMarginFromImage(image, values, true, localStride,
                globalStride);
        copyTopBottomMarginFromImage(image, values, false, localStride,
                globalStride);
        if (blockIdx.y == 0) {
            copyMarginCornerZeroes(values, 0, localStride);
            copyMarginCornerZeroes(values, 3, localStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, true, localStride);
        } else if (blockIdx.y == gridDim.y - 1) {
            copyMarginCornerZeroes(values, 1, localStride);
            copyMarginCornerZeroes(values, 2, localStride);
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginZeroes(values, false, localStride);
        } else {
            copyMarginCornerFromImage(image, values, 0, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 1, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 2, localStride,
                    globalStride);
            copyMarginCornerFromImage(image, values, 3, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, true, localStride,
                    globalStride);
            copyLeftRightMarginFromImage(image, values, false, localStride,
                    globalStride);
        }
    }
}

__global__ void convoluteMultiThreadedSharedMemory(float* image, float* output) {
    copyImageWithPadddingToSharedMemory(image);
    int idx = threadIdx.x * 4 + blockIdx.x*blockDim.x * 4;
    int idy = threadIdx.y * 4 + blockIdx.y*blockDim.y * 4;
    __syncthreads();
//    assert(idx*IMAGE_SIZE+idy < IMAGE_SIZE*IMAGE_SIZE);

    for (int i = 0; i < 4; i++) {
    	for (int j =0; j < 4; j++) {
    		output[(idy+i) * IMAGE_SIZE + idx+j] = dotProductSharedMem(i, j);
    	}
    }
}



void fill2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                h_Data[i * w + j] = i * w + j;
            }
        }
}

void show2D(float*h_Data, int h, int w) {
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                std::cout << h_Data[i * w + j] << "\t";
            }
            std::cout << std::endl;
        }
}


int main(int argc, char *argv[])
{
    float *h_Image, *h_Kernel, *d_Image, *d_Output;

    h_Image = (float*)malloc(227 * 227 * sizeof(float));
    h_Kernel = (float*)malloc(11 * 11 * sizeof(float));

    fill2D(h_Image, IMAGE_SIZE, IMAGE_SIZE);
    fill2D(h_Kernel, KERNEL_SIZE, KERNEL_SIZE);

//    std::cout << "Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);

    checkCudaErrors(cudaMalloc(&d_Image,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
    checkCudaErrors(cudaMalloc(&d_Output,IMAGE_SIZE*IMAGE_SIZE*sizeof(float)));
//    checkCudaErrors(cudaMalloc(&kernel,KERNEL_SIZE*KERNEL_SIZE*sizeof(float)));

    checkCudaErrors(cudaMemcpy(d_Image, h_Image, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyHostToDevice));
//    checkCudaErrors(cudaMemcpy(kernel, h_Kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpyToSymbol(kernel, h_Kernel, KERNEL_SIZE*KERNEL_SIZE*sizeof(float), 0, cudaMemcpyHostToDevice));


    dim3 gridDim(28, 28, 1);
    dim3 blockDim(8, 8, 1);


    //convoluteMultiThreadedSharedMemory<<<gridDim, blockDim, (blockDim.x + (KERNEL_SIZE-1))* (blockDim.x + (KERNEL_SIZE-1)) * sizeof(float)>>>(d_Image, d_Output, IMAGE_SIZE, KERNEL_SIZE);
    for (int i = 0; i < 1; i++) {
        convoluteMultiThreadedSharedMemory<<<gridDim, blockDim>>>(d_Image, d_Output);
    }


    checkCudaErrors(cudaMemcpy(h_Image, d_Output, IMAGE_SIZE * IMAGE_SIZE * sizeof(float), cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaMemcpy(h_Kernel, d_Kernel, 227 * 227 * sizeof(float), cudaMemcpyDeviceToHost));

//    std::cout << "After Image = " << std::endl;
//    show2D(h_Image, 227, 227);
//    std::cout << "After Kernel = " << std::endl;
//    show2D(h_Kernel, 11, 11);


    return 0;
}


