/*
 * ConvolutionTest.cpp
 *
 *  Created on: Dec 31, 2015
 *      Author: hsahota
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include <cuda_runtime.h>

#include "error_util.h"


texture<float, 2, cudaReadModeElementType> texImage;

__global__ void kernel(unsigned int width, unsigned int height, float* dOutput) {
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

    dOutput[x*width + y] = tex2D(texImage, x, y);
}

int main(int argc, char *argv[])
{

    unsigned int height = 8, width = 8;
    unsigned int size = height * width * sizeof(float);

    float* h_image = (float*)malloc(size);

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            h_image[i*width + j] = (float)(i + j);
        }
    }

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    cudaArray *cuArray;

    checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, width, height));
    checkCudaErrors(cudaMemcpyToArray(cuArray, 0, 0, h_image, size, cudaMemcpyHostToDevice));

    texImage.addressMode[0] = cudaAddressModeBorder;
    texImage.addressMode[1] = cudaAddressModeBorder;
    texImage.filterMode = cudaFilterModePoint;
    texImage.normalized = false;

    checkCudaErrors(cudaBindTextureToArray(texImage, cuArray, channelDesc));

    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(height / dimBlock.x, width/dimBlock.y, 1);

    float *dOutput = NULL;
    checkCudaErrors(cudaMalloc((void**) &dOutput, size));

    float* hOutput = (float*)malloc(size);

    printf("output = \n");
    for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printf("%f\t", hOutput[i*width + j]);
            }
            printf("\n");
        }


    kernel<<<dimGrid, dimBlock>>>(width, height, dOutput);
    printf("kernel finished.\n");

    checkCudaErrors(cudaMemcpy(hOutput, dOutput, size, cudaMemcpyDeviceToHost));


    printf("output = \n");
    for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printf("%f\t", hOutput[i*width + j]);
            }
            printf("\n");
        }

    printf("input = \n");

    for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printf("%f\t", h_image[i*width + j]);
            }
            printf("\n");
        }

    return 0;
}


