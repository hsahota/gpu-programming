#include <stdio.h>

#include"error_util.h"

texture<float, 1, cudaReadModeElementType> texInput;

__global__ void copyKernel(float*output, int n) {
	for (int i = 0; i < n; i++) {
		output[i] = tex1Dfetch(texInput, i);  //tex1D() is only for textures tied to an underlying cuda array
	}
}

int main(int argc, char*argv[]) {

	const int WIDTH = 8, HEIGHT = 8;

	float* hInput = (float*)malloc(sizeof(float) * WIDTH);
	float*hOutput = (float*)malloc(sizeof(float) * WIDTH);

	for (int i = 0; i < WIDTH; i++) {
		hInput[i] = (float)i + 1;
	}

	float* dInput = NULL, *dOutput = NULL;

	size_t pitch = 0;

	checkCudaErrors(cudaMallocPitch(&dInput, &pitch, WIDTH, HEIGHT));

	printf("pitch=%d\n", pitch);

	//TODO

	checkCudaErrors(cudaMalloc((void**)&dInput, sizeof(float)*WIDTH));
	checkCudaErrors(cudaMalloc((void**)&dOutput, sizeof(float)*WIDTH));

	cudaMemcpy(dInput, hInput, sizeof(float)*WIDTH, cudaMemcpyHostToDevice);

//	cudaBindTexture(&offset, texInput, dInput, sizeof(float)*WIDTH);
//
//
//	copyKernel<<<1,1>>>(dOutput, WIDTH);
//
//	cudaMemcpy(hOutput, dOutput, sizeof(float)*WIDTH, cudaMemcpyDeviceToHost);
//	printf("\nInput = ");
//
//	for (int i = 0; i < WIDTH; i++) {
//			printf("%f\t",hInput[i]);
//		}
//	printf("\nOutput = ");
//	for (int i = 0; i < WIDTH; i++) {
//		printf("%f\t",hOutput[i]);
//	}

	return 0;
}
