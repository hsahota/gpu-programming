all: cublasapiexamples cudnnconvolutiontests myconvolutionimplementations baiduspeech cublasapiexamples texturememoryexample

cublasapiexamples: common
	(cd CublasAPIExamples; make)
	
baiduspeech: common
	(cd BaiduSpeech; make)

common:
	(cd Common; make)
	
cudnnconvolutiontests: common
	(cd CudnnConvolutionTests; make)

myconvolutionimplementations: common
	(cd MyConvolutionImplementations; make)
	
texturememoryexample:
	(cd TextureMemoryExample; make)

clean:
	(cd Common; make clean)
	(cd CublasAPIExamples; make clean)
	(cd CudnnConvolutionTests; make clean)
	(cd MyConvolutionImplementations; make clean)
	(cd BaiduSpeech; make clean)
	(cd CublasAPIExamples; make clean)
